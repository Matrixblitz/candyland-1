// contains functions and data used by the candyland game

// onload behavior
// if a form has been submitted, the names will be 
// read from the query string. This will result in
// two changes to the page: unneeded p tags (for
// players) will be made invisible and default
// player names will be replaced with those submitted
// in the form

// candyland data


// the object containing the pieces and their locations
var pieces = {allain:{x:0,y:0}, jordan:{x:0,y:0}, nick:{x:0,y:0}, alfie:{x:0,y:0}};

// the deck of cards
var deckLength = 9;

// whether game is currently running
var isGameRunning = false;

// players using the game
var activePlayers = [true, true, true, true];

// currently active player; a number from 0-3,
// the actual range is not necessarily continuous
var currentPlayer = 0;

var lastPlayer = -1;

// restores the game to its default appearance
function refreshDefaults(){

  //set most of the menu and the form to be
  //invisible
  document.getElementById('playerForm').style.display='none';
  
  activePlayers = [true, true, true, true];

  currentPlayer = 0;

  document.getElementById('p1Info').innerHTML = "Player 1";
  document.getElementById('p2Info').innerHTML = "Player 2";
  document.getElementById('p3Info').innerHTML = "Player 3";
  document.getElementById('p4Info').innerHTML = "Player 4";

  highlightPlayer(currentPlayer, "pink");
  highlightPlayer(1, "powderblue");
  highlightPlayer(2, "powderblue");
  highlightPlayer(3, "powderblue");
}

// playerId is a number in 0-3 that corresponds to an
// index of the activePlayers array
function highlightPlayer(playerId, toColor){
  
  var id = playerId + 1;

  document.getElementById("p" + id + "Info").style.backgroundColor = toColor; 
  
}

// startNewGame()
// this function is used by the "New Game" button 
function startNewGame(){

  if (isGameRunning) refreshDefaults();
  isGameRunning = true;

  // turn on display of form below game
  document.getElementById('playerForm').style.display='block';
  
}

function drawCardAndCompleteTurn(){

  // randomly select a card from the "deck"
  var cardNumber = Math.floor(Math.random()*deckLength);

  // send random number to display
  // undisplay previous card
  displayCard(cardNumber);

  // decide where piece should go
  var newPiecePosition = computeNewPosition(cardNumber, pieces, currentPlayer);     

  // move the piece of the current player to new
  // position
  movePiece(newPiecePosition, pieces, currentPlayer);

  if (hasWon(currentPlayer, pieces)){
  
    window.alert(playerNames[currentPlayer] + " has won!");
    startNewGame();
  }

  else {
    
    highlightPlayer(currentPlayer, "powderblue");
    // change gui and internal settings for next player
    currentPlayer = switchTurn(currentPlayer, activePlayers);
  }

}

function computeNewPosition(number, pieceObject, currPlayer){

  return 0;
}

function switchTurn(playerId, players){


  var i = 1;
  while (!players[(playerId+i) % 4]){
    console.log("Id is " + (playerId+i)%4); 
    i = i+1;
  }

  highlightPlayer((playerId+i)%4, "pink");
   return (playerId+i)%4;
}


function movePiece(position){

  // position should be an object such as
  // {"x":11, "y":9}
}

function hasWon(currentPlayer, piecesArray){

  var winStatus = false;

  // determine whether the current player's piece
  // is at the end of the board

  return winStatus;
}

function displayCard(cardNumber){

 // color cards could change fillstyle of card canvas
 // to that color and print the color name in center
 
}

function validateName(name){

  if (name == "" || name == null){
    
    return;
  }

  if(name.length > 12){
    
    window.alert("Names must be shorter than 12 characters");
    gameStateReloading = true;
    startNewGame();
  }

  if (name.search(/[^a-z\d\s:]/i) != -1){
  
    window.alert("Names may contain only letters and digits");
    gameStateReloading = true;
    startNewGame();
  }

}
